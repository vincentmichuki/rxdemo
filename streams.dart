//https://www.burkharts.net/apps/blog/fundamentals-of-dart-streams/
//https://codingwithjoe.com/learn/dart/

import 'dart:async';

void main() async {
  Stream getSt() async* {
    for(var i=0;i<10;i++) {
      yield i;
    }
  }
  Future<void> printThings(Stream<String> data) async {
    await for (var x in data) {
      print(x);
    }
  }

  printThings(Stream<String>.value("ok Nop")); // prints "ok".

//  Stream.value(5).listen(print);
//  (()async* {yield 34;}()).listen(print);

//  StreamController _stream = StreamController();
//  Stream stream = numStream(_stream);
//  stream.listen((x) {
//    print(x);
//  }).onError((err) {
//    print('Error occured here');
//  });
////  stream.map((val) {
////    return 'Joseph' * val;
////  }).listen((val) {
////    print(val);
////  });
//  for(int i = 0; i<=10;i++) {
//    await Future.delayed(Duration(seconds: 2), () {
//      if(i == 5) {
//        _stream.addError(Error());
//      } else {
//        _stream.add(i);
//      }
//    });
//  }

//var strc = StringCache();
//int y = 3;
//takeFunc((y) {
//  return y;
//});
}

void takeFunc(void compute(int x)) {}

abstract class Cache<T> {
  T getByKey(String key);

  void setByKey(String key, T value);
}

class StringCache extends Cache<String> {
  @override
  getByKey(String key) {
    // TODO: implement getByKey
    return null;
  }

  @override
  void setByKey(String key, value) {
    // TODO: implement setByKey
  }
}

Stream numStream(StreamController _stream) {
  return _stream.stream.asBroadcastStream();
}
