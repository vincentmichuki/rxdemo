
void main() {
  var myFuture = Future(() => 10);
  myFuture.then((val) {
    print(val);
    return throw 'An error ocurred';
  }).catchError((err) {
    print(err);
  });
}
