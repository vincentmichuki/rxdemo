
import 'dart:async';

class Person {
  Person({this.personAge});

  int personAge;
  List observers = [];

  void set age(int age) {
    if(personAge  == age) return;
    personAge = age;
    notify();
  }

  int get age => personAge;

  void subscribe(observer) {
    observers.add(observer);
  }

  void notify() {
    observers.forEach((observer) {
      observer.printAge(personAge);
    });
  }
}

class PersonObserver {
  void printAge(int age) {
    print(age);
  }
}


void main() {
  int counter = 25;
  var person = Person(personAge: counter);
  person.subscribe(PersonObserver());
  Timer.periodic(Duration(seconds: 3), (Timer t){
    counter++;
    person.age = counter;
    if(counter == 32){
      t.cancel();
    }
  });
}
